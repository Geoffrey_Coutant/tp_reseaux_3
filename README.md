# TP3 : On va router des trucs

## 1. Echange ARP

### Générer des requêtes ARP

***Effectuer un ping d'une machine à l'autre***
```
$ ping 192.168.212.12
PING 192.168.212.12 (192.168.212.12) 56(84) bytes of data.
64 bytes from 192.168.212.12: icmp_seq=1 ttl=64 time=1.02 ms
64 bytes from 192.168.212.12: icmp_seq=2 ttl=64 time=0.696 ms
64 bytes from 192.168.212.12: icmp_seq=3 ttl=64 time=0.503 ms
```

***observer les tables ARP des deux machines***

Pour john :
```
$ ip neigh show
192.168.212.1 dev enp0s6 lladdr 1e:57:dc:a5:51:64 REACHABLE
192.168.212.12 dev enp0s6 lladdr b2:52:f6:d6:93:08 STALE
fe80::1c57:dcff:fea5:5164 dev enp0s6 lladdr 1e:57:dc:a5:51:64 STALE
```
L'adresse MAC de marcel est **b2:52:f6:d6:93:08**

Pour marcel : 
```
$ ip neigh show
192.168.212.11 dev enp0s6 lladdr de:17:f9:5b:7c:11 STALE
192.168.212.1 dev enp0s6 lladdr 1e:57:dc:a5:51:64 REACHABLE
fe80::1c57:dcff:fea5:5164 dev enp0s6 lladdr 1e:57:dc:a5:51:64 STALE
```
L'adresse MAC de john est **de:17:f9:5b:7c:11**

***prouvez que l'info est correcte***

**une commande pour voir la MAC de marcel dans la table ARP de john**
```
$ ip neigh show 192.168.212.12
192.168.212.12 dev enp0s6 lladdr b2:52:f6:d6:93:08 STALE
```

**une commande pour afficher la MAC de marcel, depuis marcel**
```
$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether b2:52:f6:d6:93:08 brd ff:ff:ff:ff:ff:ff
```

## 3. DHCP

### 1. Mise en place du serveur DHCP

***Sur la machine john, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").***

